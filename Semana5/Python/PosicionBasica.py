class PosicionBasica():
    __descripcion__:str=""
    __abreviatura__:str=""
    
    def __init__(self, descripcion: str, abreviatura:str):
        """Constructor

        Args:
            descripcion (str): _description_
            abreviatura (str): _description_
        """
        self.__descripcion__ = descripcion
        self.__abreviatura__ = abreviatura
        
    def getDescripcion(self):
        return self.__descripcion__
    
    def setDescripcion(self, descripcion):
        self.__descripcion__ = descripcion
        
    def getAbreviatura(self):
        return self.__abreviatura__
    
    def setAbreviatura(self, abreviatura):
        self.__abreviatura__ = abreviatura
    
    
        