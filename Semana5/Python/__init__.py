from PosicionBasica import PosicionBasica
from PosicionEspecifica import PosicionEspecifica
from Ubicacion import Ubicacion



def crearPosicionesBasicas( listaPos):
    portero =  PosicionBasica("Portero","Por")
    listaPos.append(portero)
    defensa =  PosicionBasica("Defensa","Def")
    listaPos.append(defensa)
    medio =  PosicionBasica("Medio","Med")
    listaPos.append(medio)
    delantero =  PosicionBasica("Delantero","Del")
    listaPos.append(delantero)

def crearUbicacion(listaUbic):
    derecha =  Ubicacion("Derecha","D")
    listaUbic.append(derecha)
    izquierda =  Ubicacion("Izquierda","I")
    listaUbic.append(izquierda)
    centro =  Ubicacion("Centro","c")
    listaUbic.append(centro)
    
if __name__=='__main__':
    

    listaPos = list()
    listaUbic = list()
    crearPosicionesBasicas(listaPos)
    crearUbicacion(listaUbic)
    PosEsp =  PosicionEspecifica("Lateral Derecho","LD",listaPos[1], listaUbic[0])
    