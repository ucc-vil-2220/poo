from PosicionBasica import PosicionBasica
from Ubicacion import Ubicacion
class PosicionEspecifica():
    __descripcion__ : str
    __abreviatura__ : str
    __posBasica__ : PosicionBasica
    __ubicacion__ : Ubicacion
    
    def __init__(self, descripcion: str,abreviatura: str, 
                 posBasica: PosicionBasica, ubicacion: Ubicacion ) :
        self.__descripcion__ =  descripcion
        self.__abreviatura__ = abreviatura
        self.__posBasica__ =  posBasica
        self.__ubicacion__ = ubicacion
    
    
    def getDescripcion(self):
        return self.__descripcion__
    
    def setDescripcion(self, descripcion):
        self.__descripcion__ = descripcion
        
    def getAbreviatura(self):
        return self.__abreviatura__
    
    def setAbreviatura(self, abreviatura):
        self.__abreviatura__ = abreviatura
        
    def getUbicacion(self):
        return self.__ubicacion__
    
    def setUbicacion(self, ubicacion):
        self.__ubicacion__ = ubicacion
        
    def getPosBasica(self):
        return self.__posBasica__
    
    def setPosBasica(self, posBasica):
        self.__posBasica__ = posBasica