from turtle import color


class Perro:
    __color__ = ""
    __raza__ = ""
    __nombre__ = ""
    __edad__ = 0
    __altura__ = 0.0
    
    
    def __init__(self,color:str, nombre:str, edad:int, altura:float, raza="Criollo"):
        """Constructor Perro

        Args:
            color (str): color del perro
            nombre (str): nombre del perro
            edad (int): edad en años del perro
            altura (float): Altura en centimetros desde el suelo
            raza (str, optional): Raza identificativa. Defaults to "Criollo".

        Returns:
            Perro: _description_
        """
        self.__color__ = color
        self.__nombre__ = nombre
        self.__raza__ = raza
        self.__edad__ = edad
        self.__altura__ = altura
    
    def color(self):
        return self.__color__
    
    def __getattribute__(self, __name: str) -> Any:
        pass
    
    def __setattr__(self, __name: str, __value: Any) -> None:
        pass



firulay = Perro(color="Naranja",raza="Pastor",nombre="Firulay", edad=5,altura=0.8)
print(firulay.color())