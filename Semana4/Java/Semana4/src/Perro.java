public class Perro{
    private int id;
    private String nombre;
    private String raza;
    private int edad;
    private Double altura;

    /* Constructor Vacio */
    public Perro() {
    }
    
    /**
     * Clase Perro
     * @param nombre ---> String
     * @param raza ---> String
     * @param edad ---> int
     * @param altura ---> double
     */
    public Perro(String nombre, String raza, int edad, Double altura) {
        this.id=1;
        this.nombre = nombre;
        this.raza = raza;
        this.edad = edad;
        this.altura = altura;
    }

    public String getNombre() {
        return nombre;
    }

    public int getId() {
        return id;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public Double getAltura() {
        return altura;
    }

    public void setAltura(Double altura) {
        this.altura = altura;
    }

    @Override
    public String toString() {
        return "Perro [altura=" + altura + ", edad=" + edad + ", id=" + id + ", nombre=" + nombre + ", raza=" + raza
                + "]";
    }

    

}