public class Gato {
    private String raza;
    private String color;
    private int edad;
    private double altura;
    
    public Gato() {
    }

    public Gato(String raza, String color, int edad, double altura) {
        this.raza = raza;
        this.color = color;
        this.edad = edad;
        this.altura = altura;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    @Override
    public String toString() {
        return "Gato [altura=" + altura + ", color=" + color + ", edad=" + edad + ", raza=" + raza + "]";
    }

    
    
}
