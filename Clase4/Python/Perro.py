class Perro:
    color = ""
    raza = ""
    nombre = ''
    edad = 0
    altura = 0.0
    despierto = True

    def __init__(self, color, nombre, edad, altura, raza = 'Criollo') :
        self.color = color
        self.raza = raza
        self.nombre =  nombre
        self.edad = edad
        self.altura = altura

    def ladrar():
        print('GUAAUU Guau')

    def comer():
        print("El perro come concentrado o sobras")
    
    def dormir():
        if despierto:
            despierto = False
        else:
            despierto = True
    
    def correr():
        print("El perro esta cansado")


Sanson = Perro("Negro", "Sanson", 5,16.8,)
print(Sanson)