public class Perro {
    public String color;
    public String raza;
    public String nombre;
    public int edad;
    public Double altura;
    public Boolean despierto = true;

    //Metodo Constructor//
    public Perro(String color, String raza, String nombre, int edad, 
                 Double altura){
        this.color = color;
        this.raza = raza;
        this.nombre = nombre;
        this.edad =  edad;
        this.altura = altura;
    }

    public Perro(){
        this.color ="";
        this.raza="";
        this.nombre="";
        this.edad=0;
        this.altura=0d;
    }

    public void ladrar(){
        System.out.println("GUAUU Guau");
    }

    public void comer(){
        System.out.println("El perro come concentrado o sobras?");
    }

    public void dormir(){
        if (despierto) {
            despierto = false;
        }
        else{
            despierto = true;
        }
    }

    public void correr(){
        System.out.println("Estoy cansado de correr");
    }
}
