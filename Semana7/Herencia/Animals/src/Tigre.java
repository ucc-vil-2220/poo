public class Tigre extends Animal {
    private String raza;

    public Tigre(String color, int edad, float altura, String raza) {
        super(color, edad, altura);
        this.raza = raza;
    }
    
    public Tigre() {
        super();
        this.raza = "";
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    @Override
    public String toString() {
        return super.toString()+ "Tigre [raza=" + raza + "]";
    }
    
    public void rugir(){
        System.out.println("GRrrrr Grrrrr GRRRR...");
    }

    public void cazar(){
        System.out.println("Cazando..... ");
    }
}
