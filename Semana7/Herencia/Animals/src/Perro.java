public class Perro extends Animal {
    private String nombre;
    private String raza;
    private String dueno;

    public Perro(String color, int edad, float altura, String nombre, String raza, String dueno) {
        super(color, edad, altura);
        this.nombre = nombre;
        this.raza = raza;
        this.dueno = dueno;
    }

    public Perro() {
        super();
        this.nombre = "";
        this.raza = "";
        this.dueno = "";
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getDueno() {
        return dueno;
    }

    public void setDueno(String dueno) {
        this.dueno = dueno;
    }

    @Override
    public String toString() {
        return super.toString()+"Perro [dueno=" + dueno + ", nombre=" + nombre + ", raza=" + raza + "]";
    }

    public void ladrar(){
        System.out.println("Guau Guau Guau...Grr Grr");
    }
    
    public void sentarse(){
        System.out.println("Perro Sentado...");
    }
    
}
