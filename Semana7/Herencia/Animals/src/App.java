public class App {
    public static void main(String[] args) throws Exception {
        Gato tom = new Gato("Negro", 6, 0.45f, "Tom", "Criollo", "Jerry");
        tom.comer("Raton");
        tom.dormir();
        System.out.println("Cantidad "+Animal.cantidad);
        System.out.println( tom.toString());
        Perro firulay = new Perro("Naranja", 5, .8f, "Firulay", "Criollo", "UCC");
        firulay.ladrar();
        System.out.println( firulay.toString());
        System.out.println("Cantidad "+Animal.cantidad);
        /* Animal et = new Animal("Verde", 80, 100);
        et.comer("Enzalada"); */

    }
}
