public class Gato extends Animal {
    private String nombre;
    private String raza;
    private String dueno;

    public Gato(String color, int edad, float altura, String nombre, String raza, String dueno) {
        super(color, edad, altura);
        this.nombre = nombre;
        this.raza = raza;
        this.dueno = dueno;
    }

    public Gato() {
        super();
        this.nombre = "";
        this.raza = "";
        this.dueno = "";
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getDueno() {
        return dueno;
    }

    public void setDueno(String dueno) {
        this.dueno = dueno;
    }

    @Override
    public String toString() {
        return super.toString()+" Gato [dueno=" + dueno + ", nombre=" + nombre + ", raza=" + raza + "]";
    }

    public void maullar(){
        System.out.println("MIAau MIAau MIAau...Grr Grr");
    }
    
    public void asearse(){
        System.out.println("Gato aseando...");
    }
}
