public abstract class Animal {
    private String color;
    private int edad;
    private float altura;
    private boolean estado = true;
    public static int cantidad =0;

    

    public Animal(String color, int edad, float altura) {
        this.color = color;
        this.edad = edad;
        this.altura = altura;
        cantidad ++;
    }

    public Animal() {
        this.color = "";
        this.edad = 0;
        this.altura = 0;
        cantidad ++;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public float getAltura() {
        return altura;
    }

    public void setAltura(float altura) {
        this.altura = altura;
    }
    
    public void comer(String comida){
        System.out.println("el animal come "+comida);
    }
    
    public void dormir(){
        if (this.estado == true){
            System.out.println("Durmiendo ZZZZZZZzzzzzzz");
        }
          else{
            System.out.println("Despierto");
        }  
        this.estado=false; 
    }
    

    @Override
    public String toString() {
        return "Animal [altura=" + altura + ", color=" + color + ", edad=" + edad + ", estado=" + estado + "]";
    }
}
