from Animal import Animal

class Tigre(Animal):
    __raza__ = ""
    
    def __init__(self, raza,  color, edad, altura):
        super().__init__(color, edad, altura)
        self.__raza__ = raza
        
    def __str__(self) -> str:
        print(type(super().__str__()))
        cadena = f"Tigre [raza={self.__raza__}]"+super().__str__()
        
        return  cadena
    
    def getRaza(self):
        return self.__raza__
    
    def setRaza(self, raza):
        self.__raza__ =  raza
        
    def rugir(self):
        print("Grrrurr Gruuu")
    
    def cazar(self, victima):
        print("el tigre caza ", victima)