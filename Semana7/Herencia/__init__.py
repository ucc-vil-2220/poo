from Animal import Animal
from Perro import Perro
from Gato import Gato
from Tigre import Tigre

if __name__=="__main__":
    firulay = Perro("Firulay", "Criollo", "UCC", "Amarillo", 5, 0.8)
    firulay.dormir()
    firulay.ladrar()
    garfield = Gato("Garfield", "Montes", "Jhon", "Naranja", 8, 0.45)
    garfield.comer("Lazagna")
    tono = Tigre("Bengala", "Naranja", 70, 1.80)
    tono.dormir()
    print(tono)
    print(garfield)
    print(firulay)
    alien =  Animal("Verde", 5000, 1.7)
    print(alien)