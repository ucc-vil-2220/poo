from Animal import Animal

class Perro(Animal):
    __nombre__ = ""
    __raza__ = ""
    __dueno__ = ""
    
    def __init__(self, nombre, raza, dueno, color, edad, altura):
        super().__init__(color, edad, altura)
        self.__nombre__ =  nombre
        self.__raza__ = raza
        self.__dueno__ =  dueno
        
    def __str__(self) -> str:
        return f"Perro [dueno={self.__dueno__}, nombre={self.__nombre__}, raza={self.__raza__}]"+super().__str__()
   
    def getNombre(self):
        return self.__nombre__
    
    def setNombre(self, nombre):
        self.__nombre__= nombre
        
    def getRaza(self):
        return self.__raza__
    
    def setRaza(self, raza):
        self.__raza__= raza
    
    def getDueno(self):
        return self.__dueno__
    
    def setDueno(self, dueno):
        self.__dueno__= dueno 
        
    def ladrar(self):
        print("Guau guau")
        
    def sentarse(self):
        print("Perro sentado....")