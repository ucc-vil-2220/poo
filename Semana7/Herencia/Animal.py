


from abc import ABC


class Animal(ABC):
    __color__ = ""
    __edad__ = 0
    __altura__ = 0.0
    __estado__ = True
    
    def __init__(self, color, edad, altura):
        self.__color__ = color
        self.__edad__ =  edad
        self.__altura__ =  altura
    
    def __str__(self) -> str:
        return f"Animal [altura={self.__altura__},  color={self.__color__}, edad={self.__edad__}, estado={self.__estado__} ]"
        
    def getColor(self): 
        return self.__color__
    

    def setColor(self, color): 
        self.__color__ = color
    

    def getEdad(self):
        return self.__edad__
    

    def setEdad(self, edad): 
        self.__edad__ = edad
    

    def getAltura(self): 
        return self.__altura__
    

    def setAltura(self, altura): 
        self.__altura__ = altura
    
    
    def comer(self,comida):
        print("el animal come ",comida)
    
    
    def dormir(self):
        if (self.__estado__ == True):
            print("Durmiendo ZZZZZZZzzzzzzz")
        else:
            print("Despierto")
        self.__estado__=False 
    
    
