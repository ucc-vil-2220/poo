public class Rectangulo implements FigGeometricas {
    private double base;
    private double altura;
    
    public Rectangulo(double base, double altura) {
        this.base = base;
        this.altura = altura;
    }

    public Rectangulo() {
    }

    @Override
    public double Area() {
        // TODO Auto-generated method stub
        return this.base*this.altura;
    }

    @Override
    public double Perimetro() {
        // TODO Auto-generated method stub
        return 2*(this.base+this.altura);
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    @Override
    public String toString() {
        return "Rectangulo [altura=" + altura + ", base=" + base + "]";
    }
    
}
