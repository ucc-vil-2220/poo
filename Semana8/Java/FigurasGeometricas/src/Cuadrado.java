public class Cuadrado implements FigGeometricas {
    private int lado;
    

    public Cuadrado() {
    }

    public Cuadrado(int lado) {
        this.lado = lado;
    }

    @Override
    public double Area() {
        return this.lado*this.lado;
    }

    @Override
    public double Perimetro() {
        return this.lado*4;
    }

    public int getLado() {
        return lado;
    }

    public void setLado(int lado) {
        this.lado = lado;
    }

    @Override
    public String toString() {
        return "Cuadrado [lado=" + lado + "]";
    }
    
}
