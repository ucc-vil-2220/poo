public interface FigGeometricas {
    /**
     * Area, calcula el area de la figura
     * @return double
     */
    public double Area();

    /**
     * Perimetro Calcula el perimetro de la figura
     * @return double
     */
    public double Perimetro();
}
