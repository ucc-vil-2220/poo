public class TrianguloRect implements FigGeometricas {
    private double base;
    private double altura;
    public TrianguloRect() {
    }
    public TrianguloRect(double base, double altura) {
        this.base = base;
        this.altura = altura;
    }
    @Override
    public double Area() {
        return this.base*this.altura/2;
    }
    @Override
    public double Perimetro() {
        double hipotenusa = Math.sqrt(Math.pow(this.base, 2)+(this.altura*this.altura));
        return this.base+this.altura+hipotenusa;
    }
    public double getBase() {
        return base;
    }
    public void setBase(double base) {
        this.base = base;
    }
    public double getAltura() {
        return altura;
    }
    public void setAltura(double altura) {
        this.altura = altura;
    }
    @Override
    public String toString() {
        return "TrianguloRect [altura=" + altura + ", base=" + base + "]";
    }
}
