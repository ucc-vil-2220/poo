public class Circulo implements FigGeometricas {
    private double radio;
    private final double PI = Math.PI;
    
    public Circulo() {
    }

    public Circulo(double radio) {
        this.radio = radio;
    }

    @Override
    public double Area() {
        // TODO Auto-generated method stub
        return PI*this.radio*this.radio;
    }
    @Override
    public double Perimetro() {
        // TODO Auto-generated method stub
        return 2*PI*this.radio;
    }
    public double getRadio() {
        return radio;
    }
    public void setRadio(double radio) {
        this.radio = radio;
    }
    
    @Override
    public String toString() {
        return "Circulo [radio=" + radio + "]";
    }
}
